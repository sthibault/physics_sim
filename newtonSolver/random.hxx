//
// Created by Steve Thibault on 2/18/23.
//
#pragma once

#include <random>
#include <SFML/Graphics.hpp>

namespace patternsoft
{

extern std::random_device rd;
extern std::mt19937 gen;

extern std::uniform_real_distribution<float> dis;

auto randomRange = [](auto min, auto max) { return dis(gen) * (max - min) + min; };
auto randomVelocityComponent = [](auto min, auto max)->auto { return dis(gen) * (max - min) + min; };
auto randomPositionComponent = [](auto max) { return dis(gen) * (max); };


template <typename TVector>
sf::Vector2<TVector> MakeVector(TVector x, TVector y)
{
	return sf::Vector2<TVector>{x, y};
};
}
