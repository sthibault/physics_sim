//
// Created by Steve Thibault on 2/18/23.
//

#include "object.hxx"
#include <cmath>
#include <iostream>

#include "vector.hxx"

namespace patternsoft
{
	uint64_t Object::lastId {0};

	bool Object::Update()
	{
		bool boundHit {false};

		auto renderSize = renderTarget.getSize();

		velocity += gravity;
		position += velocity;

		if (position.x + size.x >= renderSize.x)
		{
			velocity.x *= -1;
			position.x = renderSize.x - size.x;
			boundHit = true;
		}
		else if (position.x < 0)
		{
			velocity.x *= -1;
			position.x = 0;
			boundHit = true;
		}

		if (position.y + size.y >= renderSize.y)
		{
			velocity.y *= -1;
			position.y = renderSize.y - size.y;
			boundHit = true;
		}
//		else if (position.y < 0)
//		{
//			velocity.y *= -1;
//			position.y = 0;
//			boundHit = true;
//		}

		object.setPosition(position.x, position.y);
		text.setPosition(position.x + radius/2, position.y + radius/2);

		return boundHit;
	}

	void Object::CheckCollision(Object& other)
	{
		if (id == other.id)
		{
			return;
		}

		const float responceCoef = 0.75f;
		auto deltaX1 = this->position - other.position;

		auto distanceSq = deltaX1.x * deltaX1.x + deltaX1.y * deltaX1.y;
		auto collideDistance = this->radius + other.radius;
		auto collideDistanceSq = collideDistance * collideDistance;
 		if (distanceSq < (collideDistanceSq))
		{
			auto deltaX2 = other.position - this->position;
			auto deltaV1 = this->velocity - other.velocity;
			auto deltaV2 = other.velocity - this->velocity;

			auto distance = std::sqrt(distanceSq);
			const sf::Vector2f n = deltaX1 / distance;
			const float massRatio1 = 2 * this->radius / (this->radius + other.radius);
			const float massRatio2 = 2 * other.radius / (this->radius + other.radius);
			const auto moveAverage = 0.5f * (distance - collideDistance);
			this->position -= n * massRatio2 * moveAverage;
			other.position += n * massRatio1 * moveAverage;
			this->velocity = this->velocity - (massRatio2 *
			                                   Dot(deltaV1, deltaX1) *
			                                   (deltaX1) * (1.0f/
			                                   std::pow(Magnitude(deltaX1), 2.0f)));
			other.velocity = other.velocity - (massRatio1 *
			                                   Dot(deltaV2, deltaX2) *
			                                   (deltaX2) * (1.0f/
			                                                std::pow(Magnitude(deltaX2), 2.0f)));
		}
	}

	void Object::Render()
	{
		renderTarget.draw(object);
		setText(text, font);
		renderTarget.draw(text);
	}
}