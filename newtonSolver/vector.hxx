//
// Created by Steve Thibault on 2/27/23.
//

#pragma once
#include <iostream>
#include <cmath>

#include <SFML/Graphics.hpp>

namespace patternsoft
{

	template <typename TIntegral>
	class Vector2 : sf::Vector2<TIntegral>
	{
	public:
		/* Vector Addition Overloads --------------------------------------------*/
		Vector2<TIntegral> operator+(Vector2<TIntegral>& other);
		Vector2<TIntegral>& operator+=(Vector2<TIntegral>& other);

		/* Vector Subtraction Overloads -----------------------------------------*/
		Vector2<TIntegral> operator-(Vector2<TIntegral>& other);
		Vector2<TIntegral>& operator-=(Vector2<TIntegral>& other);

		/* Vector Multiplication Overloads --------------------------------------*/
		Vector2<TIntegral> operator*(TIntegral& scale);
		Vector2<TIntegral>& operator*=(TIntegral& scale);

		/// \brief Magnitude
		/// \return The length of the vector ignoring sign or direction known as
		/// 				|| vector2 ||
		TIntegral Magnitude();

		/// \brief Dot Product
		/// \param other The vector to Dot Product with this one
		/// \return A scalar representing the Dot Product of two Vectors, or
		/// 				The sum  of the products of the components of the vector.
		/// 				known as < v1, v2 >
		TIntegral Dot(Vector2<TIntegral>& other);
	};

	/* Vector Addition Overloads ----------------------------------------------*/
	template <typename TIntegral>
	Vector2<TIntegral> Vector2<TIntegral>::operator+(Vector2<TIntegral>& other)
	{
		return Vector2<TIntegral> {this->x + other.x, this->y + other.y};
	}
	template<typename TIntegral>
	Vector2<TIntegral>& Vector2<TIntegral>::operator+=(Vector2<TIntegral>& other)
	{
		if (other == this)
		{
			return;
		}
		this->x += other.x;
		this->y += other.y;

		return this;
	}

	/* Vector Subtraction Overloads -------------------------------------------*/

	template<typename TIntegral>
	Vector2<TIntegral> Vector2<TIntegral>::operator-(Vector2<TIntegral>& other)
	{
		return Vector2<TIntegral> {this->x - other.x, this->y - other.y};
	}

	template<typename TIntegral>
	Vector2<TIntegral>& Vector2<TIntegral>::operator-=(Vector2<TIntegral>& other)
	{
		if (other == this)
		{
			return;
		}
		this->x -= other.x;
		this->y -= other.y;

		return this;
	}

	template<typename TIntegral>
	Vector2<TIntegral> Vector2<TIntegral>::operator*(TIntegral& scale)
	{
		return Vector2<TIntegral>{this->x * scale, this->y * scale};
	}

	template<typename TIntegral>
	Vector2<TIntegral>& Vector2<TIntegral>::operator*=(TIntegral& scale)
	{
		this->x *= scale;
		this->y *= scale;

		return this;
	}

	template<typename TIntegral>
	TIntegral Vector2<TIntegral>::Magnitude()
	{
		return static_cast<TIntegral>(std::sqrt( (this->x * this->x) + (this->y * this->y)));
	}

	template<typename TIntegral>
	TIntegral Vector2<TIntegral>::Dot(Vector2<TIntegral>& other)
	{
		return (this->x * other.x) + (this->y * other.y);
	}

	/* ostream overload -------------------------------------------------------*/
	template <typename TIntegral>
	std::ostream& operator<<(std::ostream& os, Vector2<TIntegral>& vector)
	{
		os << "Vector2{ x: " << vector.x << " y: " << vector.y << " }";
	}

	/* Free function Dot Product between two arbitrary Vector2<x> -------------*/
	template <typename TIntegral>
	TIntegral Dot(const Vector2<TIntegral>& v1, const Vector2<TIntegral>& v2)
	{
		return (v1.x * v2.x) + (v1.y * v2.y);
	}

	template <typename TIntegral>
	TIntegral Dot(const sf::Vector2<TIntegral>& v1, const sf::Vector2<TIntegral>& v2)
	{
		return (v1.x * v2.x) + (v1.y * v2.y);
	}

	template <typename TIntegral>
	TIntegral Magnitude(const sf::Vector2<TIntegral>& v)
	{
		return static_cast<TIntegral>(std::sqrt((v.x * v.x) + (v.y * v.y)));
	}

	template <typename TIntegral>
	sf::Vector2<TIntegral> operator*(sf::Vector2<TIntegral>& left, TIntegral right)
	{
		return sf::Vector2<TIntegral> { left.x * right, left.y * right };
	}

	template <typename TIntegral>
	sf::Vector2<TIntegral> operator*(TIntegral left, sf::Vector2<TIntegral>& right)
	{
		return sf::Vector2<TIntegral> { right.x * left, right.y * left };
	}

	template <typename TIntegral>
	sf::Vector2<TIntegral>& operator*=(sf::Vector2<TIntegral>& left, TIntegral right)
	{
		left.x *= right;
		left.y *= right;
		return  left;
	}

}// namespace ptnsft
