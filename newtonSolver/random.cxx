//
// Created by Steve Thibault on 2/18/23.
//
#include "random.hxx"

namespace patternsoft
{
	std::random_device rd;
	std::mt19937 gen(rd());

	std::uniform_real_distribution<float> dis(0.0, 1.0);
}// namespace ptnsft