//
// Created by Steve Thibault on 2/18/23.
//

#include "random.hxx"
#include <exception>
#include <ostream>
#include <SFML/Graphics.hpp>

#pragma once

namespace patternsoft
{
	class Object
	{
	public:
		explicit Object(sf::RenderTarget& renderTarget) :
		    id{lastId++},
		    renderTarget {renderTarget},
		    radius{randomRange(50.0f, 50.0f)},
		    position {1600, 400},
		    velocity {MakeVector(randomRange(-5.0f, 5.0f), randomRange(-5.0f, 5.0f))},
		    object {radius, 128},
		    text{},
		    size {object.getGlobalBounds().width, object.getGlobalBounds().height}
		{
			object.setOrigin(0.0, 0.0);
			object.setFillColor(color);
			object.setOutlineColor(sf::Color::Red);
			object.setOutlineThickness(1);
			setText(text, font);
		}

		Object(sf::RenderTarget& renderTarget, sf::Vector2f position, sf::Vector2f velocity, float r) :
		    id{lastId++},
		    renderTarget {renderTarget},
		    radius {r},
		    position {position},
		    velocity {velocity},
		    object {radius, 128},
		    size {object.getGlobalBounds().width, object.getGlobalBounds().height}
		{
			object.setOrigin(0.0, 0.0);
			object.setFillColor(color);
			object.setOutlineColor(sf::Color::Red);
			object.setOutlineThickness(1);
			setText(text, font);
		}

		bool Update();
		void Render();
		void CheckCollision(Object& object);
		const sf::Vector2f& Size() const
		{
			return size;
		}

		const sf::Vector2f& Position() const
		{
			return position;
		}

		uint64_t id;
		static uint64_t lastId;

	private:
		std::uint64_t maxAge {std::numeric_limits<u_int64_t>::max()};
		std::uint64_t age {0};
		bool dead {false};
		sf::RenderTarget& renderTarget;
		float radius;
		sf::Vector2f position {};
		sf::Vector2f velocity {};
		sf::Vector2f gravity {0, 5};
		sf::CircleShape object;
		sf::Text text;
		sf::Vector2f size;
		sf::Color color {sf::Color::Black};
		sf::Font font;

		void setText(sf::Text& text, sf::Font& font)
		{
			if (!font.loadFromFile("/System/Library/Fonts/Monaco.ttf"))
			{
				throw std::runtime_error("Could not load font");
			}
			text.setString(std::to_string(id));
			text.setCharacterSize(50);
			text.setOrigin(0, 0);
			text.setFillColor(sf::Color::White);
			text.setFont(font);
			text.setStyle(sf::Style::None);
		}
	};
}