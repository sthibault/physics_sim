#include "object.hxx"
#include "random.hxx"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <random>

void setText(sf::Text& text, sf::Font& font)
{
	if (!font.loadFromFile("/System/Library/Fonts/Monaco.ttf"))
	{
		throw std::runtime_error("Could not load font");
	}

	text.setString("This is Text");
	text.setCharacterSize(50);
	text.setOrigin(0, 0);
	text.setFillColor(sf::Color::Black);
	text.setFont(font);
	text.setStyle(sf::Style::None);
}

int main()
{
	using namespace patternsoft;

	// Create window
	constexpr int32_t window_width  = 3200;
	constexpr int32_t window_height = 1600;

	sf::ContextSettings settings;
	settings.antialiasingLevel = 1;
	sf::RenderWindow window(sf::VideoMode(window_width, window_height),
	                        "Verlet", sf::Style::Default, settings);
	const uint32_t frame_rate = 60;
	window.setFramerateLimit(frame_rate);
	sf::Color background{sf::Color::White};

	sf::Clock clock;

	std::vector<Object> objects;
//	objects.emplace_back(window, sf::Vector2f{50, window_height - 50}, sf::Vector2f{10, 0}, 50);
//	objects.emplace_back(window, sf::Vector2f{window_width - 50,  window_height - 50}, sf::Vector2f{-10, 0}, 50);

	// Main loop
	while (window.isOpen())
	{
		sf::Event event{};
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			{
				Object o{window};
				objects.push_back(o);
			}
		}

		window.clear(background);
		background = sf::Color::White;

		for (std::size_t i{0}; i < objects.size(); ++i)
		{
			auto& object = objects[i];
			if (objects[i].Update()) {}

			for (std::size_t j{i + 1}; j < objects.size(); ++j)
			{
				auto& other = objects[j];
				object.CheckCollision(other);
			}
			objects[i].Render();
		}

		window.display();
	}

	return 0;
}
